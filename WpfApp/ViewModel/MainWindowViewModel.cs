﻿using Microsoft.Toolkit.Mvvm.ComponentModel;

namespace WpfApp.ViewModel
{
    public class MainWindowViewModel : ObservableObject
    {
        private string _title;

        public MainWindowViewModel()
        {
            Title = "WpfApp";
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
    }
}
