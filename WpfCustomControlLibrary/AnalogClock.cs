﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfCustomControlLibrary
{
    [TemplatePart(Name = "PART_HourHand", Type = typeof(Line))]
    [TemplatePart(Name = "PART_MinuteHand", Type = typeof(Line))]
    [TemplatePart(Name = "PART_SecondHand", Type = typeof(Line))]
    public class AnalogClock : Clock
    {
        private Line _hourHand;
        private Line _minuteHand;
        private Line _secondHand;

        static AnalogClock()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AnalogClock), new FrameworkPropertyMetadata(typeof(AnalogClock)));
        }

        public override void OnApplyTemplate()
        {
            _hourHand = Template.FindName("PART_HourHand", this) as Line;
            _minuteHand = Template.FindName("PART_MinuteHand", this) as Line;
            _secondHand = Template.FindName("PART_SecondHand", this) as Line;

            base.OnApplyTemplate();
        }

        protected override void OnTimeChanged(DateTime dt)
        {
            UpdateHandAngles(dt);
            base.OnTimeChanged(dt);
        }

        private void UpdateHandAngles(DateTime dt)
        {
            _hourHand.RenderTransform = new RotateTransform(dt.Hour / 12.0 * 360, 0.5, 0.5);
            _minuteHand.RenderTransform = new RotateTransform(dt.Minute / 60.0 * 360, 0.5, 0.5);
            _secondHand.RenderTransform = new RotateTransform(dt.Second / 60.0 * 360, 0.5, 0.5);
        }
    }
}
