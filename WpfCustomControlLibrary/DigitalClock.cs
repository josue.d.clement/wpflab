﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace WpfCustomControlLibrary
{
    [TemplatePart(Name = "PART_Colon", Type = typeof(UIElement))]
    public class DigitalClock : Clock
    {
        static DigitalClock()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DigitalClock), new FrameworkPropertyMetadata(typeof(DigitalClock)));
        }

        private UIElement _colon;

        public static readonly DependencyProperty ColonBlinkProperty =
            DependencyProperty.Register("ColonBlink", typeof(bool), typeof(DigitalClock), new PropertyMetadata(true));

        public bool ColonBlink
        {
            get { return (bool)GetValue(ColonBlinkProperty); }
            set { SetValue(ColonBlinkProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            _colon = Template.FindName("PART_Colon", this) as UIElement;

            base.OnApplyTemplate();
        }

        protected override void OnTimeChanged(DateTime newTime)
        {
            if (_colon != null)
            {
                if (ColonBlink && !ShowSeconds)
                    _colon.Visibility = _colon.IsVisible ? Visibility.Hidden : Visibility.Visible;
                else
                    _colon.Visibility = Visibility.Visible;
            }

            base.OnTimeChanged(newTime);
        }
    }
}
