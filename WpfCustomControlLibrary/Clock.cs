﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace WpfCustomControlLibrary
{
    [TemplateVisualState(Name = "Day", GroupName = "TimeStates")]
    public class Clock : Control
    {
        public static readonly DependencyProperty TimeProperty =
            DependencyProperty.Register(nameof(Time), typeof(DateTime), typeof(Clock), new PropertyMetadata(DateTime.Now));

        public static readonly DependencyProperty ShowSecondsProperty =
            DependencyProperty.Register(nameof(ShowSeconds), typeof(bool), typeof(Clock), new PropertyMetadata(true));

        public static RoutedEvent TimeChangedEvent =
            EventManager.RegisterRoutedEvent(nameof(TimeChanged), RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<DateTime>), typeof(Clock));

        public DateTime Time
        {
            get { return (DateTime)GetValue(TimeProperty); }
            set { SetValue(TimeProperty, value); }
        }

        public bool ShowSeconds
        {
            get { return (bool)GetValue(ShowSecondsProperty); }
            set { SetValue(ShowSecondsProperty, value); }
        }

        public event RoutedPropertyChangedEventHandler<DateTime> TimeChanged
        {
            add { AddHandler(TimeChangedEvent, value); }
            remove { RemoveHandler(TimeChangedEvent, value); }
        }

        public override void OnApplyTemplate()
        {
            //Binding showSecondHandBinding = new Binding
            //{
            //    Path = new PropertyPath(nameof(ShowSeconds)),
            //    Source = this,
            //    Converter = new BooleanToVisibilityConverter()
            //};

            //_secondHand.SetBinding(VisibilityProperty, showSecondHandBinding);

            OnTimeChanged(DateTime.Now);

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += (s, e) => OnTimeChanged(DateTime.Now);
            timer.Start();

            base.OnApplyTemplate();
        }

        protected virtual void OnTimeChanged(DateTime newTime)
        {
            UpdateTimeState(newTime);
            RaiseEvent(new RoutedPropertyChangedEventArgs<DateTime>(Time, newTime, TimeChangedEvent));
            Time = newTime;
        }

        private void UpdateTimeState(DateTime dt)
        {
            if (dt.Hour > 6 && dt.Hour < 18)
                VisualStateManager.GoToState(this, "Day", false);
            else
                VisualStateManager.GoToState(this, "Night", false);
        }
    }

    //public delegate void TimeChangedEventHandler(object sender, TimeChangedEventArgs args);

    //public class TimeChangedEventArgs : RoutedEventArgs
    //{
    //    public DateTime NewTime { get; set; }

    //    public TimeChangedEventArgs() { }
    //    public TimeChangedEventArgs(RoutedEvent routedEvent) : base(routedEvent) { }
    //    public TimeChangedEventArgs(RoutedEvent routedEvent, object source) : base(routedEvent, source) { }
    //}
}
